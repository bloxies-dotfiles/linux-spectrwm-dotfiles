# Linux Spectrwm Dotfiles

My Linux Spectrwm Dotfiles! Managed with YADM

Fonts will have to be installed manually, Icons/Themes are included

If you want an easier/less tedious way to install Arch, try out :


[Archfi](https://github.com/MatMoul/archfi)


I would like to thank adi1090x for the custom "faded_city" theme base at : 

[Slim Custom Themes](https://github.com/adi1090x/slim_themes)


If you are going to use this, make sure you replace/remove the .bloxieinstaller file for your current distribution check update method or way of checking for updates on Arch.

You can auto install my personal setup in Arch with : (you must have Yay package manager by default)

wget https://gitlab.com/bloxies-dotfiles/linux-spectrwm-dotfiles/raw/master/.bloxieinstaller


And then 


sh .bloxieinstaller



Required things : spectrwm compton-tryone-git oblogout playerctl dunst acpi conky j4-dmenu-desktop pulseaudio pulseaudio-alsa noto-fonts ttf-twemoji-color noto-fonts-cjk 

My Personal System Things : xorg networkmanager slim youtube-dl perl-bash-completion udisks2 mpv ntfs-3g gvfs flameshot feh pamac-aur

My Personal Applications : termite lxappearance pavucontrol chromium thunderbird gimp qtwebflix-git gtk-youtube-viewer-git nemo mousepad xarchiver steam minecraft-technic-launcher ripcord spotify
