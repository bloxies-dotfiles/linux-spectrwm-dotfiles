#!/bin/sh

updates_checked=$(pamac checkupdates | wc -l)
updates_amount=$((updates_checked - 1))
if [ "$updates_amount" -gt "0" ]; then
	echo "| Updates : $updates_amount"
else
	echo ""
fi



