#!/bin/sh

player_status=$(playerctl --player=spotify status 2> /dev/null)
if [ "$player_status" = "Playing" ]; then
	echo "| Playing : $(playerctl --player=spotify metadata artist) - $(playerctl --player=spotify metadata title) on Spotify"
elif [ "$player_status" = "Paused" ]; then
	echo "| Paused : $(playerctl --player=spotify metadata artist) - $(playerctl --player=spotify metadata title) on Spotify"
else
	echo ""
fi



