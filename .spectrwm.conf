# PLEASE READ THE MAN PAGE BEFORE EDITING THIS FILE!
# https://htmlpreview.github.io/?https://github.com/conformal/spectrwm/blob/master/spectrwm.html
# NOTE: all rgb color values in this file are in hex! see XQueryColor for examples

workspace_limit	= 7
# focus_mode				= default
# focus_close				= previous
# focus_close_wrap			= 1
# focus_default				= last
# spawn_position			= next
# workspace_clamp			= 1
# warp_focus				= 1
# warp_pointer				= 1

# Window Decoration
border_width				= 2
color_focus					= rgb:40/00/80
# color_focus_maximized		= yellow
# color_unfocus				= rgb:88/88/88
# color_unfocus_maximized	= rgb:88/88/00
region_padding				= 5
tile_gap					= 10

# Region containment
# Distance window must be dragged/resized beyond the region edge before it is
# allowed outside the region.
# boundary_width 			= 50

# Remove window border when bar is disabled and there is only one window in workspace
# disable_border			= 1

# Bar Settings
bar_enabled					= 1
bar_border_width			= 2
#bar_border[1]				= rgb:40/00/80
bar_border[1]				= rgb:10/17/5
# bar_border_unfocus[1]		= rgb:00/40/40
bar_color[1]				= rgb:10/17/5
# bar_color_selected[1]		= rgb:00/80/80
# bar_font_color[1]			= rgb:a0/a0/a0
# bar_font_color_selected	= black
bar_font					= Noto Sans
bar_action      			= conky
bar_justify					= center
bar_format					= +L | %R | %A %d %B %Y | %D +A
workspace_indicator			= listcurrent,listactive,printnames,markcurrent,markurgent
# bar_at_bottom				= 1
# stack_enabled				= 1
clock_enabled				= 1
clock_format				= %a %b %d %R %Z %Y
# iconic_enabled			= 0
# maximize_hide_bar			= 0
# window_class_enabled		= 0
# window_instance_enabled	= 0
# window_name_enabled		= 1
# verbose_layout			= 1
urgent_enabled				= 1
# urgent_collapse			= 1

# Dialog box size ratio when using TRANSSZ quirk; 0.3 < dialog_ratio <= 1.0
# dialog_ratio				= 0.6

# Split a non-RandR dual head setup into one region per monitor
# (non-standard driver-based multihead is not seen by spectrwm)
# region					= screen[1]:1280x1024+0+0
# region					= screen[1]:1280x1024+1280+0

# Customize workspace layout at start
# layout					= ws[1]:4:0:0:0:vertical
# layout					= ws[2]:0:0:0:0:horizontal
# layout					= ws[3]:0:0:0:0:fullscreen
# layout					= ws[4]:4:0:0:0:vertical_flip
# layout					= ws[5]:0:0:0:0:horizontal_flip

layout					= ws[4]:7:0:0:0:vertical

# Set workspace name at start
name						= ws[1]:WWW
name						= ws[2]:DEV
name						= ws[3]:SYS
name						= ws[4]:STEM
name						= ws[5]:GAME
name						= ws[6]:CHAT
name						= ws[7]:MUSIC

# Mod key, (Windows key is Mod4) (Apple key on OSX is Mod2)
modkey 						= Mod4

# This allows you to include pre-defined key bindings for your keyboard layout.
keyboard_mapping 			= ~/.spectrwm_keybinds.conf

# PROGRAMS

# Main Keybinds:
program[logoutmenu]			= oblogout
program[lock]				= slimlock
program[screenshot]			= flameshot gui
program[termite]			= termite
program[runmenu]			= j4-dmenu-desktop [--dmenu="dmenu_run $dmenu_bottom -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected"] [--term="termite"]
program[musicplaypause] 	= dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause

# Bind Main Keybinds:
bind[logoutmenu]			= MOD+Shift+e
bind[lock]				= MOD+Shift+l
bind[screenshot]			= MOD+Shift+s
bind[termite]				= MOD+Return	# Terminal Keybinding
bind[runmenu]				= MOD+d		# Run Menu Keybinding
bind[musicplaypause]		= MOD+p		# Play and Pause Toggle on currently open music player (eg. Spotify)

# QUIRKS
# Default quirks, remove with: quirk[class:name] = NONE
#quirk[stalonetray:stalonetray]	= FLOAT + ANYWHERE + NOFOCUSCYCLE + NOFOCUSONMAP + OBEYAPPFOCUSREQ + MINIMALBORDER

quirk[Steam:Steam]			= WS[4]

quirk[Geany]				= WS[2] 

quirk[Ripcord:ripcord]		= WS[6] 

# Quirks for system applications

quirk[Oblogout]				= FLOAT + ANYWHERE + NOFOCUSCYCLE + OBEYAPPFOCUSREQ + MINIMALBORDER

quirk[flameshot]				= FLOAT

# Autolaunch applications
autorun						= ws[1]:termite -e elinks google.com

autorun						= ws[3]:termite
autorun						= ws[3]:nemo

autorun						= ws[4]:steam -no-browser

autorun						= ws[6]:ripcord

autorun						= ws[7]:spotify
